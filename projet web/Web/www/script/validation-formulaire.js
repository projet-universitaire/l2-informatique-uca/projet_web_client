document.addEventListener('DOMContentLoaded', function () {
    // Récupération des éléments du formulaire
    const form = document.querySelector('form');
    const lastNameInput = document.getElementById('lastname');
    const firstNameInput = document.getElementById('firstname');
    const birthdateInput = document.getElementById('birthdate');
    const usernameInput = document.getElementById('username');
    const passwordInput = document.getElementById('userpwd');
    const emailInput = document.getElementById('useremail');

    // Ajout d'un écouteur d'événement pour le formulaire
    form.addEventListener('submit', function (event) {
        if (!validateForm()) {
            event.preventDefault(); // Empêche la soumission du formulaire si les champs ne sont pas valides
        }
    });

    // Fonction de validation du formulaire
    function validateForm() {
        let isValid = true;

        // Validation du nom
        isValid = isValid && validateInputWithMessage(lastNameInput, /.+/, 'Le nom est obligatoire.');

        // Validation du prénom
        isValid = isValid && validateInputWithMessage(firstNameInput, /.+/, 'Le prénom est obligatoire.');

        // Validation de la date de naissance
        isValid = isValid && validateDateWithMessage(birthdateInput, 'La date de naissance n\'est pas valide.');

        // Validation du nom d'utilisateur
        isValid = isValid && validateInputWithMessage(usernameInput, /.{6,}/, 'Le nom d\'utilisateur doit avoir au moins 6 caractères.');

        // Validation du mot de passe
        isValid = isValid && validatePasswordWithMessage(passwordInput, 'Le mot de passe doit avoir au moins 12 caractères, une majuscule, une minuscule, un chiffre et un caractère spécial.');

        // Validation de l'adresse email
        isValid = isValid && validateEmailWithMessage(emailInput, 'L\'adresse email n\'est pas valide.');


        return isValid;
    }

    // Fonction de validation générique avec messages d'erreur
    function validateInputWithMessage(input, pattern, errorMessage) {
        const value = input.value.trim();
        const isValid = pattern.test(value);
        const errorMessageElement = document.getElementById(`${input.id}-error`);

        updateFieldStatus(input, isValid);

        if (!isValid) {
            errorMessageElement.textContent = errorMessage;
        } else {
            errorMessageElement.textContent = '';
        }

        return isValid;
    }


    // Fonction de validation de la date de naissance
    function validateDateWithMessage(input, errorMessage) {
        const value = input.value.trim();
        const dateRegex = /^(0[1-9]|[12][0-9]|3[01])\/(0[1-9]|1[0-2])\/\d{4}$/;
        const isValidFormat = value === '' || dateRegex.test(value);
        if (value===''){
            isValidFormat = true;
        }
        else{
           isValidDate = isValidFormat && isValidDateObject(value);
        }
        
        const errorMessageElement = document.getElementById(`${input.id}-error`);

        updateFieldStatus(input, isValidDate);

        if (!isValidDate) {
            errorMessageElement.textContent = errorMessage;
        } else {
            errorMessageElement.textContent = '';
        }

        return isValidDate;
    }

    // Fonction pour vérifier la validité de la date avec l'objet Date
    function isValidDateObject(dateString) {
        const parts = dateString.split('/');
        const day = parseInt(parts[0], 10);
        const month = parseInt(parts[1], 10);
        const year = parseInt(parts[2], 10);
        if (day === 29 && month === 1) {
            return isLeapYear(year);
        }
        const dateObject = new Date(year, month - 1, day);
        return !isNaN(dateObject);
    }

    // Fonction pour vérifier si l'année est bissextile
    function isLeapYear(year) {
        return (year % 4 === 0 && (year % 100 !== 0 || year % 400 === 0));
    }

    // Fonction de validation du mot de passe avec message d'erreur
    function validatePasswordWithMessage(champ, errorMessage) {
        const valeur = champ.value.trim();
        const regexMotDePasse = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[-_!?:;.*&$])[A-Za-z\d-_!?:;.*&$]{12,}$/;
        const estValide = regexMotDePasse.test(valeur);
        const errorMessageElement = document.getElementById(`${champ.id}-error`);

        updateFieldStatus(champ, estValide);

        if (!estValide) {
            errorMessageElement.textContent = errorMessage;
        } else {
            errorMessageElement.textContent = '';
        }

        return estValide;
    }

    // Fonction de validation de l'adresse email avec message d'erreur
    function validateEmailWithMessage(input, errorMessage) {
        const value = input.value.trim();
        const emailRegex = /.+@.+\..+/;
        const isValid = emailRegex.test(value);
        const errorMessageElement = document.getElementById(`${input.id}-error`);

        updateFieldStatus(input, isValid);

        if (!isValid) {
            errorMessageElement.textContent = errorMessage;
        } else {
            errorMessageElement.textContent = '';
        }

        return isValid;
    }

    // Fonction de mise à jour de l'état du champ
    function updateFieldStatus(input, isValid) {
        input.classList.remove('invalid', 'valid');
        input.classList.add(isValid ? 'valid' : 'invalid');
    }
});



