document.addEventListener('DOMContentLoaded', function () {
  const darkModeToggle = document.getElementById('dark-mode-toggle');
  const body = document.body;

  // Vérifie si le mode sombre est activé dans le stockage local
  const isDarkMode = localStorage.getItem('darkMode') === 'enabled';

  // Applique le mode sombre si nécessaire
  if (isDarkMode) {
      enableDarkMode();
  }

  // Ajoute un écouteur d'événement pour le clic sur l'icône de la lune
  darkModeToggle.addEventListener('click', () => {
      if (body.classList.contains('dark-mode')) {
          disableDarkMode();
      } else {
          enableDarkMode();
      }
  });

  // Fonction pour activer le mode sombre
  function enableDarkMode() {
      body.classList.add('dark-mode');
      localStorage.setItem('darkMode', 'enabled');
      darkModeToggle.innerHTML = '<img src="../img/soleil.png" alt="mode claire" title="passer en mode claire">';
  }

  // Fonction pour désactiver le mode sombre
  function disableDarkMode() {
      body.classList.remove('dark-mode');
      localStorage.setItem('darkMode', null);
      darkModeToggle.innerHTML = '<img src="../img/lune.png" alt="mode sombre" title = "passer en mode sombre">';
  }
});