$(document).ready(function() {
    $('#chatIcon').click(function() {
      $('#chatBox').slideToggle();
    });
    
    $('#closeChat').click(function() {
      $('#chatBox').slideUp();
    });
  });
  




$(document).ready(function() {
    const chatArea = $('#chatArea');
    const messageInput = $('#messageInput');
    const sendButton = $('#sendButton');

    // Fonction pour envoyer un message
    function sendMessage() {
        const message = messageInput.val().trim();

        if (message === '') {
            return;
        }

        $.ajax({
            url: '../htbin/chatsend.py',
            method: 'POST',
            data: { msg: message },
            success: function(response) {
                if (response.num === 0) {
                    messageInput.val(''); // Effacer le champ de saisie
                    fetchMessages(); // Mettre à jour la zone de discussion
                } else {
                    displayMessage(response.msg); // Afficher le message d'erreur
                }
            },
            error: function(xhr, status, error) {
                console.error('Erreur lors de l\'envoi du message:', error);
            }
        });
    }

    // Fonction pour récupérer les messages
    function fetchMessages() {
        $.ajax({
            url: '../htbin/chatget.py',
            method: 'GET',
            success: function(messages) {
                displayMessages(messages);
            },
            error: function(xhr, status, error) {
                console.error('Erreur lors de la récupération des messages:', error);
            }
        });
    }

    // Fonction pour afficher les messages
    function displayMessages(messages) {
        chatArea.empty(); // Effacer le contenu actuel de la zone de discussion
        $.each(messages, function(index, message) {
            chatArea.append(`<div>${message.user} (${message.date} ${message.time}): ${message.msg}</div>`);
        });
    }

    // Fonction pour afficher un message d'erreur
    function displayMessage(errorMessage) {
        chatArea.append(`<div class="error">${errorMessage}</div>`);
    }

    // Écouteur d'événement pour le clic sur le bouton "Envoyer"
    sendButton.click(sendMessage);

    // Écouteur d'événement pour la touche "Entrée" dans le champ de saisie du message
    messageInput.keydown(function(event) {
        if (event.key === 'Enter') {
            sendMessage();
        }
    });

    // Charger les messages initiaux lors du chargement de la page
    fetchMessages();
});




/*document.addEventListener('DOMContentLoaded', function () {
    const messageInput = document.getElementById('messageInput');
    const sendButton = document.getElementById('sendButton');
    const chatArea = document.getElementById('chatArea');

    // Fonction pour envoyer un message
    function sendMessage() {
        const message = messageInput.value.trim();

        if (message === '') {
            return;
        }

        const xhr = new XMLHttpRequest();
        const url = '../htbin/chatsend.py';
        const formData = new FormData();
        formData.append('msg', message);

        xhr.open('POST', url);
        xhr.onload = function() {
            if (xhr.status === 200) {
                const response = JSON.parse(xhr.responseText);
                if (response.num === 0) {
                    // Message envoyé avec succès
                    messageInput.value = ''; // Effacer le champ de saisie
                    fetchMessages(); // Mettre à jour la zone de discussion
                } else {
                    // Erreur lors de l'envoi du message
                    displayMessage(response.msg); // Afficher le message d'erreur
                }
            } else {
                console.error('Erreur lors de l\'envoi du message:', xhr.statusText);
            }
        };

        xhr.send(formData);
    }

    // Fonction pour récupérer les messages
    function fetchMessages() {
        const xhr = new XMLHttpRequest();
        const url = '../htbin/chatget.py';

        xhr.open('GET', url);
        xhr.onload = function() {
            if (xhr.status === 200) {
                const messages = JSON.parse(xhr.responseText);
                displayMessages(messages);
            } else {
                console.error('Erreur lors de la récupération des messages:', xhr.statusText);
            }
        };

        xhr.send();
    }

    // Fonction pour afficher les messages
    function displayMessages(messages) {
        chatArea.innerHTML = ''; // Effacer le contenu actuel de la zone de discussion
        messages.forEach(function(message) {
            const messageElement = document.createElement('div');
            messageElement.textContent = `${message.user} (${message.date} ${message.time}): ${message.msg}`;
            chatArea.appendChild(messageElement);
        });
    }

    // Fonction pour afficher un message d'erreur
    function displayMessage(errorMessage) {
        const errorElement = document.createElement('div');
        errorElement.textContent = errorMessage;
        chatArea.appendChild(errorElement);
    }

    // Écouteur d'événement pour le clic sur le bouton "Envoyer"
    sendButton.addEventListener('click', sendMessage);

    // Écouteur d'événement pour la touche "Entrée" dans le champ de saisie du message
    messageInput.addEventListener('keydown', function(event) {
        if (event.key === 'Enter') {
            sendMessage();
        }
    });

    // Charger les messages initiaux lors du chargement de la page
    fetchMessages();
});
*/