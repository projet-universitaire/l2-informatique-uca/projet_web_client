document.addEventListener('DOMContentLoaded', function () {
    const loginForm = document.getElementById('loginForm');
    const messageElement = document.getElementById('message');

    // Fonction de soumission du formulaire
    function submitForm(event) {
        event.preventDefault(); // Empêche la soumission du formulaire

        const username = document.getElementById('username').value;
        const password = document.getElementById('userpwd').value;

        const xhr = new XMLHttpRequest();
        const url = '../htbin/login.py'; 

        xhr.open('POST', url);
        xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');

        xhr.onload = function() {
            if (xhr.status === 200) {
                messageElement.innerHTML = 'Response serveur : '+xhr.responseText;
            } else {
                console.error('Erreur lors de la connexion:', xhr.statusText);
            }
        };

        const formData = 'username=' + encodeURIComponent(username) + '&userpwd=' + encodeURIComponent(password);
        xhr.send(formData);
    }

    // Ajout de l'écouteur d'événement pour la soumission du formulaire
    loginForm.addEventListener('submit', submitForm);

    // Ajout de l'écouteur d'événement pour la touche "Entrée"
    loginForm.addEventListener('keydown', function(event) {
        if (event.key === "Enter") {
            event.preventDefault();
            submitForm(event); // Appel de la fonction de soumission du formulaire
        }
    });
});
